// src/app.ts
import { PrismaClient } from "@prisma/client"

const prisma = new PrismaClient()

async function main() {
  // remove all
  await prisma.book.deleteMany()
  const firstBook = await prisma.book.create({
    data: {
      title: "My First Book",
    },
  })
  console.log("After adding First book.", await prisma.book.findMany())
  const firstBookUpdates = await prisma.book.update({
    where: { id: firstBook.id },
    data: {
      title: "My First Book!!!",
    },
  })
  console.log("After updating First book.", await prisma.book.findMany())
  await prisma.book.delete({
    where: { id: firstBook.id },
  })
  console.log("After deleting First book.", await prisma.book.findMany())
}

main()
  .catch((e) => {
    console.log(e)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
