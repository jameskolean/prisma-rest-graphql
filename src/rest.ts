// src/rest.ts
import Fastify from "fastify"
const server = Fastify({
  logger: true,
})
import authorV1Routes from "./modules/author/author.v1.routes"
import bookV1Routes from "./modules/book/book.v1.routes"
import { bookSchemas } from "./modules/book/book.schema"

async function main() {
  for (const schema of bookSchemas) {
    server.addSchema(schema)
  }

  server.register(authorV1Routes, { prefix: "/v1" })
  server.register(bookV1Routes, { prefix: "/v1" })

  // Run the server!
  server.listen({ port: 3000 }, function (err, address) {
    if (err) {
      server.log.error(err)
      process.exit(1)
    }
    console.log(`Server is now listening on ${address}`)
  })
}
main()
