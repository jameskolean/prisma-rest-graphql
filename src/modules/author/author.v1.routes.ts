// src/modules/author/author.v1.routes.ts

import { FastifyInstance } from "fastify"
import { PrismaClient } from "@prisma/client"

const prisma = new PrismaClient()

async function authorRoutes(server: FastifyInstance) {
  server.get("/author", async function (request, reply) {
    reply.send(await prisma.author.findMany())
  })
  server.post("/author", async (request, reply) => {
    const user = request.body as AuthorCreate
    reply.send(
      await prisma.author.create({
        data: {
          name: user.name,
        },
      })
    )
  })
  server.put("/author", async (request, reply) => {
    const author = request.body as AuthorUpdate
    reply.send(
      await prisma.author.update({
        data: {
          name: author.name,
        },
        where: { id: author.id },
      })
    )
  })
  server.delete("/author", async (request, reply) => {
    const author = request.body as AuthorDelete
    reply.send(
      await prisma.author.delete({
        where: { id: author.id },
      })
    )
  })
}

export default authorRoutes

interface AuthorCreate {
  name: string
}
interface AuthorDelete {
  id: string
}
interface AuthorUpdate {
  id: string
  name: string
}
