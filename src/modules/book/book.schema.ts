// src/modules/book/book.schema.ts
import { z } from "zod"
import { buildJsonSchemas } from "fastify-zod"

const bookCore = {
  title: z.string({ required_error: "Title is required." }),
}

const createBookSchema = z.object({
  ...bookCore,
})

const createBookResponseSchema = z.object({
  ...bookCore,
  id: z.string(),
})

const deleteBookSchema = z.object({
  id: z.string(),
})

const deleteBookResponseSchema = z.object({
  ...bookCore,
  id: z.string(),
})

const updateBookSchema = z.object({
  ...bookCore,
  id: z.string(),
})
const updateBookResponseSchema = z.object({
  ...bookCore,
  id: z.string(),
})

export const { schemas: bookSchemas, $ref } = buildJsonSchemas({
  createBookSchema,
  createBookResponseSchema,
  deleteBookSchema,
  deleteBookResponseSchema,
  updateBookSchema,
  updateBookResponseSchema,
})
