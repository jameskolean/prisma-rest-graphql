// src/modules/user/book.v1.routes.ts

import { FastifyInstance } from "fastify"
import { PrismaClient } from "@prisma/client"
import { $ref } from "./book.schema"

const prisma = new PrismaClient()

async function bookRoutes(server: FastifyInstance) {
  server.get("/book", async function (request, reply) {
    reply.send(await prisma.book.findMany())
  })
  server.post(
    "/book",
    {
      schema: {
        body: $ref("createBookSchema"),
        response: { 201: $ref("createBookResponseSchema") },
      },
    },
    async (request, reply) => {
      const book = request.body as BookCreate
      reply.send(
        await prisma.book.create({
          data: {
            title: book.title,
            description: book.description,
          },
        })
      )
    }
  )
  server.put(
    "/book",
    {
      schema: {
        body: $ref("updateBookSchema"),
        response: { 200: $ref("updateBookResponseSchema") },
      },
    },
    async (request, reply) => {
      const book = request.body as BookUpdate
      reply.send(
        await prisma.book.update({
          data: {
            title: book.title,
            description: book.description,
          },
          where: { id: book.id },
        })
      )
    }
  )
  server.delete(
    "/book",
    {
      schema: {
        body: $ref("deleteBookSchema"),
        response: { 200: $ref("deleteBookResponseSchema") },
      },
    },
    async (request, reply) => {
      const book = request.body as BookDelete
      reply.send(
        await prisma.book.delete({
          where: { id: book.id },
        })
      )
    }
  )
}

export default bookRoutes

interface BookCreate {
  title: string
  description?: string
}
interface BookDelete {
  id: string
}
interface BookUpdate {
  id: string
  title: string
  description?: string
}
