// prisma/seed.ts
import { PrismaClient } from "@prisma/client"

const prisma = new PrismaClient()

async function seed() {
  // create Categories
  const [fiction, nonFiction] = await Promise.all([
    prisma.category.create({
      data: {
        name: "fiction",
      },
    }),
    prisma.category.create({
      data: {
        name: "non-fiction",
      },
    }),
  ])

  // create Author and Biography
  const markTwain = await prisma.author.create({
    data: {
      name: "Mark Twain",
      biography: {
        create: {
          dob: new Date("November 30, 1835"),
        },
      },
    },
  })

  // create books
  await Promise.all([
    prisma.book.create({
      data: {
        title: "The Adventures of Tom Sawyer",
        authorId: markTwain.id,
        categories: { connect: { id: fiction.id } },
        description:
          "The Adventures of Tom Sawyer is an 1876 novel by Mark Twain about a boy growing up along the Mississippi River. It is set in the 1840s in the town of St. Petersburg, which is based on Hannibal, Missouri, where Twain lived as a boy.",
      },
    }),
    prisma.book.create({
      data: {
        title: "Adventures of Huckleberry Finn",
        authorId: markTwain.id,
        categories: { connect: { id: fiction.id } },
        description:
          "The Adventures of Huckleberry Finn, is a novel by American author Mark Twain, which was first published in the United Kingdom in December 1884 and in the United States in February 1885.",
      },
    }),
  ])
}

seed()
  .catch((e) => {
    console.log(e)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
